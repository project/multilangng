# MultiLangNG
The module for international agencies.
- Relieve pain.
- Share best practices.
- Pioneer the future.
-
## What this contains.
See [MultiLangNG-Translations](multilangng_translations/README.md).
For other ideas and contributions, [create an issue](https://www.drupal.org/node/add/project-issue/multilangng).
