# MultiLangNG Translations

## What D8 core does about config vs string translations
- All module provided config (i.e. its translatable strings), is synced back and forth between UI string translations and config language overrides.
- The idea is, that module provided strings should behave like t() defined strings in modules.

## The sad truth
- It does not work, and even is a PITA for some. There are well-known major bugs, and possibly conceptual flaws with the approach.
- The result: Corrupted config translations.
- Also, it is not possible to translate a config string in one config differently than in other places (there is no context support for a single config string, just for its schema location like "Views title", which is of very limited use).

## What this module offers
- All config (i.e. its translatable strings) is translated on the fly.
- No config overrides necessary.
- But if config language overrides exist, that is used.

## The result in practice
- Build a site, and its config.
- Get existing string translations for free for all config (not only module-provided).
- If that translation does not fit, add a config language override.
- So, the amount of config language overrides is zero for many sites, or they contain only a few contextual exceptions.
- Which makes config language overrides reviewable in practice.

## What about non-english config?
- Any non-english config string is translated using "Langcode xx" as translation context.

## How does the module do that?
- It disables config-translation-sync (On config save update string translation; On module install reset all config to default language; On config install sync config and string translation)
- It translates all config (not only module provided) on the fly via string translation (providing the "default translation").
- Any config override trumps that default translation, so contextual translation is possible.

## Remaining challenges
- Core has the assumption that all default config is in site default language.
  - We propose that every default config can have any language, and then is translatable to any other, just like content.
  - TODO: Add language selector to config forms, just like content. Decide what to do with translations to that language.
- Pruning orphan translation strings
  - Including all config may add to orphaned translation strings (but they exist anyway).
