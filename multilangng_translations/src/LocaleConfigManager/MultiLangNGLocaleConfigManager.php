<?php

namespace Drupal\multilangng_translations\LocaleConfigManager;

use Drupal\locale\LocaleConfigManager;

/**
 * This class is not used. It tells the IDE about ComposableInheritance.
 *
 * @see \Drupal\multilangng_translations\MultilangngServiceProvider::alter
 */
class MultiLangNGLocaleConfigManager extends LocaleConfigManager {

  use MultiLangNGLocaleConfigManagerTrait;

}
