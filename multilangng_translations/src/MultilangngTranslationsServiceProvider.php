<?php

namespace Drupal\multilangng_translations;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\multilangng_translations\LocaleConfigManager\MultiLangNGLocaleConfigManagerTrait;
use geeks4change\composable_inheritance\ComposableInheritance;

class MultilangngTranslationsServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container) {
    $localeConfigSubscriberDefinition = $container->getDefinition('locale.config_subscriber');
    $localeConfigSubscriberDefinition->clearTag('event_subscriber');

    $localeConfigManagerDefinition = $container->getDefinition('locale.config_manager');
    $class = ComposableInheritance::create($localeConfigManagerDefinition->getClass())
      ->useTrait(MultiLangNGLocaleConfigManagerTrait::class)
      ->class();
    $localeConfigManagerDefinition->setClass($class);
  }

}
