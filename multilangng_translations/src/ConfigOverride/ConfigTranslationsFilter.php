<?php

declare(strict_types=1);
namespace Drupal\multilangng_translations\ConfigOverride;

use Drupal\Core\TypedData\TypedDataInterface;

final class ConfigTranslationsFilter {

  /**
   * Suppress config from being translated.
   *
   * Needed because marks its yaml translatable, but suppresses translating
   * **any** string that is valid Yaml in the translation form.
   * Here we suppress translating webform.webform.*:elements
   *
   * @see webform_form_locale_translate_edit_form_alter
   *
   * @param string $name
   *   The config name.
   * @param array $keys
   *   The config item parent keys.
   * @param \Drupal\Core\TypedData\TypedDataInterface $element
   *   The typed data item to inspect.
   *
   * @return bool
   */
  public function suppressTranslateConfig(string $name, array $keys, TypedDataInterface $element): bool {
    if (str_starts_with($name, 'webform.webform.')) {
      return $keys === ['elements'];
    }
    return FALSE;
  }

}
