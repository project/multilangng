<?php

namespace Drupal\multilangng_translations\ConfigOverride;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Override config with translations, instead of syncing config override.
 *
 * @todo Investigate performance.
 */
class MultiLangNGTranslationsConfigOverride implements ConfigFactoryOverrideInterface {

  protected LanguageManagerInterface $languageManager;

  protected ConfigTranslationsOverrideProvider $configTranslationsOverrideProvider;

  protected bool $loadingOverrides = FALSE;

  /**
   * Get locale config manager only when needed from container.
   *
   * Avoid circular dependency (not resolvable via setter injection):
   *   Circular reference detected for service "extension.list.module",
   *   path: "extension.list.module -> config.factory ->
   *   multilangng_translations.config_override ->
   *   multilangng_translations.config_translations_override_provider ->
   *   config.manager -> extension.path.resolver".
   */
  protected function getConfigTranslationsOverrideProvider(): ConfigTranslationsOverrideProvider {
    if (!isset($this->configTranslationsOverrideProvider)) {
      $this->configTranslationsOverrideProvider = \Drupal::service('multilangng_translations.config_translations_override_provider');
    }
    return $this->configTranslationsOverrideProvider;
  }

  /**
   * Get language manager only when needed from container.
   *
   * Avoid circular dependency (not resolvable via setter injection):
   *   Circular reference detected for service "language_manager",
   *   path: "language_request_subscriber -> language_manager -> config.factory
   *   -> multilangng_translations.config_override".
   */
  protected function getLanguageManager(): LanguageManagerInterface {
    if (!isset($this->languageManager)) {
      $this->languageManager = \Drupal::service('language_manager');
    }
    return $this->languageManager;
  }

  /**
   * Get configuration target language.
   *
   * This should in fact use LanguageManagerInterface::getConfigOverrideLanguage,
   * but this leads to nasty and hard debug issues. How to reproduce:
   * - Set 'de' as negotiation default.
   * - (Maybe it's relevant to enable search_api and webform_views)
   * Then on `drush cr`, english labels are cached for 'de'.
   * This is because EntityTypeManager caches field definitions for
   * currentLanguage, which is "de", but configOverrideLanguage is "en",
   * because it defaults to defaultLanguage and seems to not have been set yet.
   *
   * Maybe the reason is that sometimes the listener calls currentLanguage
   * before it's initialized, in which case it falls back to 'system', but
   * later when it is initialized, configOverrideLanguage is not updated.
   * If that is the case, finishing initialization of LanguageManager should
   * trigger syncing configOverrideLanguage somehow.
   *
   * And in the end, do we need configOverrideLanguage as a separate concept
   * from currentLanguage (i.e. interface_language) at all?
   *
   * @see \Drupal\Core\Language\LanguageManagerInterface::getConfigOverrideLanguage
   * @see \Drupal\language\EventSubscriber\LanguageRequestSubscriber::setLanguageOverrides
   * @see \Drupal\language\ConfigurableLanguageManager::getCurrentLanguage
   *
   * @todo Find a fix upstream and get rid of this hack.
   */
  protected function getTargetLanguage(): LanguageInterface {
    return $this->getLanguageManager()->getCurrentLanguage();
  }

  /**
   * Load overrides.
   *
   * @see \Drupal\locale\LocaleConfigManager::getTranslatableDefaultConfig
   */
  public function loadOverrides($names): array {
    if ($this->loadingOverrides) {
      // Prevent recursion. Which may happen like that:
      // - LocaleConfigManagerExcerpt::getDefaultConfigLangcode checks if a
      //   given config is a config entity.
      // - Entity definitions are collected.
      // - Any entity definition depends on config
      //   (e.g. \commerce_order_entity_type_alter)
      // Fortunately we can assume that translation overrides do not affect
      // config entity prefix definitions in any sane way.
      return [];
    }
    $targetLangcode = $this->getTargetLanguage()->getId();
    $overrides = [];
    $this->loadingOverrides = TRUE;
    foreach ($names as $name) {
      $overrides[$name] = $this->getConfigTranslationsOverrideProvider()->getTranslatableConfig($name, $targetLangcode);
    }
    $this->loadingOverrides = FALSE;
    return $overrides;
  }

  /**
   * Create config object.
   *
   * Nothing needed here, this is completely dynamic.
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION): ?StorableConfigBase {
    return NULL;
  }

  /**
   * Get cache suffix.
   *
   * @todo Remove the need for this by returning TranslatableMarkup.
   * @see \Drupal\multilangng_translations\ConfigOverride\ConfigTranslationsOverrideProvider::getTranslatableData
   */
  public function getCacheSuffix(): ?string {
    return $this->getTargetLanguage()->getId();
  }

  /**
   * Get cacheability.
   *
   * @todo Remove the need for this by returning TranslatableMarkup.
   * @see \Drupal\multilangng_translations\ConfigOverride\ConfigTranslationsOverrideProvider::getTranslatableData
   */
  public function getCacheableMetadata($name): CacheableMetadata {
    $metadata = new CacheableMetadata();
    $metadata->setCacheContexts(['languages:language_interface']);
    return $metadata;
  }

}
