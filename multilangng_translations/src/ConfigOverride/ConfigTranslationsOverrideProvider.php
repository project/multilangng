<?php

namespace Drupal\multilangng_translations\ConfigOverride;

use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\multilangng_source_language\SourceLanguageTranslationServiceFactoryInterface;

/**
 * Config translations override provider.
 *
 * Copied and adapted some methods from LocaleConfigManager.
 *
 * @see \Drupal\locale\LocaleConfigManager
 */
class ConfigTranslationsOverrideProvider {

  public function __construct(
    protected StorageInterface $configStorage,
    protected TypedConfigManagerInterface $typedConfigManager,
    # Added in beta2, so optional to have a safe upgrade path.
    # todo: Make required in next major.
    protected ?SourceLanguageTranslationServiceFactoryInterface $translationServiceFactory,
    protected ?ConfigTranslationsFilter $configTranslationsFilter,
  ) {}


  /**
   * Adapted @see \Drupal\locale\LocaleConfigManager::getTranslatableDefaultConfig
   *
   * In contrast to that, config is used, not module provided config.
   */
  public function getTranslatableConfig(string $name, string $targetLangcode) {
    $data = $this->configStorage->read($name);
    if ($data) {
      $typed_config = $this->typedConfigManager->createFromNameAndData($name, $data);
      if ($typed_config instanceof TraversableTypedDataInterface) {
        $sourceLangcode = $data['langcode'] ?? 'en';
        $translatableData = $this->getTranslatableData($typed_config, $targetLangcode, $sourceLangcode, $name);
        // Because config is always an array.
        assert(is_array($translatableData));
        return [
            'langcode' => $targetLangcode,
          ] + $translatableData;
      }
    }
    return [];
  }

  /**
   * Adapted @see \Drupal\locale\LocaleConfigManager::getTranslatableData
   */
  protected function getTranslatableData(TypedDataInterface $element, string $targetLangcode, string $sourceLangcode, string $name, array $keys = []): array|string {
    $translatable = [];
    if ($sourceLangcode === $targetLangcode) {
      return $translatable;
    }
    elseif ($element instanceof TraversableTypedDataInterface) {
      foreach ($element as $key => $property) {
        $value = $this->getTranslatableData($property, $targetLangcode, $sourceLangcode, $name, array_merge($keys, [$key]));
        if (!empty($value)) {
          $translatable[$key] = $value;
        }
      }
    }
    else {
      // Something is only translatable by Locale if there is a string in the
      // first place.
      $value = $element->getValue();
      $definition = $element->getDataDefinition();
      if (
        !empty($definition['translatable'])
        && $value !== ''
        && $value !== NULL
        && !$this->configTranslationsFilter?->suppressTranslateConfig($name, $keys, $element)
      ) {
        // In an ideal world we could return a stringable TranslatableMarkup
        // object to have the string translated lazily. Which would fix all
        // kinds of nasty issues when translation is done before interface
        // language negotiation has finished.
        // Testing shows that this works most of the cases, but some views code
        // coughs.
        // So return an eagerly translated string here.
        $context = $definition['translation context'] ?? '';
        $maybeTranslation = $this->translationServiceFactory
          ?->forSourceLangcode($sourceLangcode)
          ?->getStringTranslation($targetLangcode, $value, $context);
        return $maybeTranslation ?: $value;
      }
    }
    return $translatable;
  }

}
