<?php

namespace Drupal\multilangng_source_language;

use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;

interface SourceLanguageTranslationServiceFactoryInterface {

  public function forSourceLangcode(string $sourceLangcode): TranslationInterface|TranslatorInterface;

}