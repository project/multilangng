<?php

declare(strict_types=1);

namespace Drupal\multilangng_source_language;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;

abstract class TranslationManagerDecoratorBase implements TranslationInterface, TranslatorInterface {

  protected TranslationInterface | TranslatorInterface $decorated;

  public function translate($string, array $args = [], array $options = []) {
    return $this->decorated->translate($string, $args, $options);
  }

  public function translateString(TranslatableMarkup $translated_string) {
    return $this->decorated->translateString($translated_string);
  }

  public function formatPlural($count, $singular, $plural, array $args = [], array $options = []) {
    return $this->decorated->formatPlural($count, $singular, $plural, $args, $options);
  }

  public function getStringTranslation($langcode, $string, $context) {
    return $this->decorated->getStringTranslation($langcode, $string, $context);
  }

  public function reset() {
    $this->decorated->reset();
  }

}
