<?php

declare(strict_types=1);

namespace Drupal\multilangng_source_language;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;

final class SourceLanguageTranslationServiceFactory implements SourceLanguageTranslationServiceFactoryInterface {

  public function __construct(
    protected TranslationInterface & TranslatorInterface $translationManager,
  ) {}

  public function forSourceLangcode(string $sourceLangcode): TranslationInterface|TranslatorInterface {
    if ($sourceLangcode && $sourceLangcode !== 'en' && $sourceLangcode !== LanguageInterface::LANGCODE_SYSTEM) {
      return new SourceLanguageTranslationManagerDecorator($sourceLangcode, $this->translationManager);
    }
    else {
      return $this->translationManager;
    }
  }

}
