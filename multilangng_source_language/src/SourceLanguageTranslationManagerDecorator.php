<?php

declare(strict_types=1);

namespace Drupal\multilangng_source_language;

use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;

final class SourceLanguageTranslationManagerDecorator extends TranslationManagerDecoratorBase {

  public function __construct(
    protected string $sourceLanguage,
    TranslationInterface | TranslatorInterface $decorated,
  ) {
    $this->decorated = $decorated;
  }


  public function translate($string, array $args = [], array $options = []) {
    $this->addSourceLanguageToContext($options['context']);
    return parent::translate($string, $args, $options);
  }

  public function getStringTranslation($langcode, $string, $context) {
    $this->addSourceLanguageToContext($context);
    return parent::getStringTranslation($langcode, $string, $context);
  }

  protected function addSourceLanguageToContext(?string &$context) {
    if ($this->sourceLanguage) {
      $context = ContextWithMetadata::parse($context ?? '')
        ->withKeyValue('srclang', $this->sourceLanguage)
        ->toString();
    }
  }

}
